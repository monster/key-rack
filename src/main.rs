#![allow(clippy::new_without_default)]

mod app;
mod data;
mod utils;
mod widgets;

use adw::prelude::*;
use app::KrApp;
use gettextrs::*;
use gtk::{gio, glib};
use gvdb_macros::include_gresource_from_dir;

const APP_ID_WITHOUT_SUFFIX: &str = "app.drey.KeyRack";
const APP_ID_SUFFIX: &str = default_env::default_env!("APPLICATION_ID_SUFFIX", "");
const APP_ID: &str = const_str::concat!(APP_ID_WITHOUT_SUFFIX, APP_ID_SUFFIX);

const LOCALEDIR: &str = default_env::default_env!("LOCALEDIR", "/usr/share/locale");

static GRESOURCE_BYTES: &[u8] = if const_str::equal!("app.drey.KeyRack", APP_ID) {
    include_gresource_from_dir!("/app/drey/KeyRack", "data/resources")
} else if const_str::equal!("app.drey.KeyRack.Devel", APP_ID) {
    include_gresource_from_dir!("/app/drey/KeyRack/Devel", "data/resources")
} else {
    panic!("Invalid DBUS_API_PATH")
};

thread_local!(
    static APPLICATION: KrApp = KrApp::new();
);

pub fn app() -> KrApp {
    APPLICATION.with(|x| x.clone())
}

fn main() {
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain("key-rack", crate::LOCALEDIR).unwrap();
    textdomain("key-rack").unwrap();

    gio::resources_register(
        &gio::Resource::from_data(&glib::Bytes::from_static(GRESOURCE_BYTES)).unwrap(),
    );

    app().run();
}
